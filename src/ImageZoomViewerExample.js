import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  Image,
  Button,
  Touchable,
} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import React from 'react';

const images = [
  {
    props: {
      source: require('../assets/image1.png'),
    },
  },
  {
    props: {
      source: require('../assets/image2.png'),
    },
  },
  {
    props: {
      source: require('../assets/image3.png'),
    },
  },
];

export default function ImageZoomViewerExample({show, onClose}) {
  return (
    <Modal visible={show} transparent={true}>
      {/* <TouchableOpacity
        onPress={onClose}
        style={{
          backgroundColor: 'transparent',
          alignItems: 'flex-end',
        }}>
        
      </TouchableOpacity> */}
      <Icon name="times" size={30} color="white" onPress={onClose} />
      <ImageViewer imageUrls={images} />
    </Modal>
  );
}

const styles = StyleSheet.create({});
