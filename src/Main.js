import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useState} from 'react';
import ImageZoomViewerExample from './ImageZoomViewerExample';
import Share from 'react-native-share';

export default function Main({navigation}) {
  const [showImagezoom, setShowImagezoom] = useState(false);

  const shareOptions = {
    title: 'Share Via',
    message: 'Test Share',
    social: Share.Social.SMS,
    recipient: '6285648822509',
  };

  const onShare = async () => {
    try {
      await Share.shareSingle(shareOptions);
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View style={styles.screen}>
      <ScrollView>
        <ImageZoomViewerExample
          show={showImagezoom}
          onClose={() => setShowImagezoom(false)}
        />
        <Text style={styles.title}>Menu</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('CarouselPage')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Snap Carousel
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => setShowImagezoom(true)}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Image Zoom Viewer
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('DatePicker')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Date Time Picker
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('NetInfo')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Net Info
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Slider')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Slider
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Progressview')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Progress View
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Progressbar')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Progress Bar Android
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Clipboard')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Clipboard
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Geolocation')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Geolocation
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('AsyncStorage')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Async Storage
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('WebView')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            WebView
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Tooltip')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Tooltip
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Skeleton')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Skeleton Placeholder
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Renderhtml')}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            HTML Render
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={onShare}>
          <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
            Share
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginBottom: 20,
  },
  button: {
    backgroundColor: 'blue',
    marginBottom: 10,
    padding: 5,
    alignItems: 'center',
    borderRadius: 10,
  },
});
