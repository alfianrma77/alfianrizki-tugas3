import {StyleSheet, Text, View, TouchableOpacity, Modal} from 'react-native';
import React, {useState} from 'react';
import RNDateTimePicker from '@react-native-community/datetimepicker';

export default function DatePicker() {
  const [showDatePicker, setShowdatePicker] = useState(false);
  const [showTimePicker, setShowtimePicker] = useState(false);
  return (
    <View style={styles.screen}>
      <Modal visible={showDatePicker} transparent={true}>
        <RNDateTimePicker
          value={new Date()}
          onChange={() => setShowdatePicker(false)}
        />
      </Modal>
      <Modal visible={showTimePicker} transparent={true}>
        <RNDateTimePicker
          value={new Date()}
          mode="time"
          display="spinner"
          onChange={() => setShowtimePicker(false)}
        />
      </Modal>
      <Text style={styles.title}>Date and Time Picker</Text>
      <TouchableOpacity
        style={styles.btnShowDate}
        onPress={() => setShowdatePicker(true)}>
        <Text style={styles.textButton}>Show Date Picker</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.btnShowDate}
        onPress={() => setShowtimePicker(true)}>
        <Text style={styles.textButton}>Show Time Picker</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'black',
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  btnShowDate: {
    backgroundColor: 'blue',
    borderRadius: 10,
    padding: 10,
    marginBottom: 15,
  },
  textButton: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
