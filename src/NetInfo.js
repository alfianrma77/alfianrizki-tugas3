import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import NetInfo from '@react-native-community/netinfo';

export default function NetInfoPage() {
  const [netInfo, setNetInfo] = useState('');

  const getNetInfo = () => {
    NetInfo.fetch().then(state => {
      alert(`Connection type: ${state.type} isConnected:${state.isConnected}`);
    });
  };
  return (
    <View style={{flex: 1, justifyContent: 'center', padding: 30}}>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.textButton} onPress={() => getNetInfo()}>
          Show Net Info
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'blue',
    padding: 5,
    borderRadius: 10,
    alignItems: 'center',
  },
  textButton: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
});
