import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import Tooltip from 'rn-tooltip';

export default function ToolTipPage() {
  return (
    <View style={styles.screen}>
      <Tooltip popover={<Text>Info here</Text>}>
        <Text>Press me</Text>
      </Tooltip>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
