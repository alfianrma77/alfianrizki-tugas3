import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {ProgressView} from '@react-native-community/progress-view';

export default function ProgresViewPage() {
  return (
    <View style={styles.screen}>
      <View>
        <Text>ProgresViewPage</Text>
        <ProgressView
          progressTintColor="orange"
          trackTintColor="#ffff"
          progress={0.7}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    padding: 30,
    alignItems: 'center',
  },
});
