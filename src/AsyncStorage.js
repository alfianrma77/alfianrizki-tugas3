import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function AsyncStoragePage() {
  const [inputText, setInputText] = useState('');
  const [savedText, setSavedText] = useState('');
  const saveData = async text => {
    try {
      await AsyncStorage.setItem('my-key', text);
    } catch (e) {
      console.log(e);
    }
  };
  const getData = async () => {
    try {
      const text = await AsyncStorage.getItem('my-key');
      if (text !== null) {
        setSavedText(text);
      }
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <View style={styles.screen}>
      <View style={styles.content}>
        <TextInput
          style={styles.input}
          placeholder="Input Data"
          onChangeText={text => setInputText(text)}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => saveData(inputText)}>
          <Text style={styles.textButton}>Save Data</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => getData()}>
          <Text style={styles.textButton}>Show Data</Text>
        </TouchableOpacity>
        <Text>{savedText}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    padding: 50,
  },
  content: {
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    width: '100%',
    marginBottom: 15,
  },
  button: {
    backgroundColor: 'blue',
    borderRadius: 10,
    width: '100%',
    marginBottom: 15,
    padding: 10,
    alignItems: 'center',
  },
  textButton: {
    fontSize: 20,
    color: 'white',
  },
});
