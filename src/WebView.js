import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {WebView} from 'react-native-webview';

export default function WebViewPage() {
  return (
    <WebView source={{uri: 'https://reactnative.dev/'}} style={{flex: 1}} />
  );
}

const styles = StyleSheet.create({});
