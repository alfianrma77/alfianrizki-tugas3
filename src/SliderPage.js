import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import Slider from '@react-native-community/slider';
import {style} from 'deprecated-react-native-prop-types/DeprecatedViewPropTypes';

export default function SliderPage() {
  const [value, setValue] = useState(0);
  return (
    <View style={styles.screen}>
      <Text style={{fontSize: 20}}>{value}</Text>
      <Slider
        style={styles.slider}
        step={1}
        minimumValue={0}
        maximumValue={100}
        minimumTrackTintColor="#FFFFFF"
        maximumTrackTintColor="#000000"
        onValueChange={setValue}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    padding: 30,
    alignItems: 'center',
  },
  slider: {
    width: '100%',
  },
});
