/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import Main from './src/Main';
import CarouselExample from './src/CarouselExample';
import ImageZoomViewerExample from './src/ImageZoomViewerExample';
import TryIcon from './src/DatePicker';
import DatePicker from './src/DatePicker';
import NetInfoPage from './src/NetInfo';
import SliderPage from './src/SliderPage';
import ProgresViewPage from './src/ProgresView';
import ProgressBarAndroid from './src/ProgressBarAndroid';
import ClipboardPage from './src/Clipboard';
import GeolocationPage from './src/Geolocation';
import AsyncStorage from './src/AsyncStorage';
import AsyncStoragePage from './src/AsyncStorage';
import WebViewPage from './src/WebView';
import ToolTipPage from './src/Tooltip';
import SkeletonPlaceholderPage from './src/SkeletonPlaceholder';
import RenderHTMLPage from './src/RenderHTML';

const Stack = createNativeStackNavigator();

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Main" component={Main} />
        <Stack.Screen name="CarouselPage" component={CarouselExample} />
        <Stack.Screen name="DatePicker" component={DatePicker} />
        <Stack.Screen name="NetInfo" component={NetInfoPage} />
        <Stack.Screen name="Slider" component={SliderPage} />
        <Stack.Screen name="Progressview" component={ProgresViewPage} />
        <Stack.Screen name="Progressbar" component={ProgressBarAndroid} />
        <Stack.Screen name="Clipboard" component={ClipboardPage} />
        <Stack.Screen name="Geolocation" component={GeolocationPage} />
        <Stack.Screen name="AsyncStorage" component={AsyncStoragePage} />
        <Stack.Screen name="WebView" component={WebViewPage} />
        <Stack.Screen name="Tooltip" component={ToolTipPage} />
        <Stack.Screen name="Skeleton" component={SkeletonPlaceholderPage} />
        <Stack.Screen name="Renderhtml" component={RenderHTMLPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
